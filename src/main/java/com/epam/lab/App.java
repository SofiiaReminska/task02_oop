package com.epam.lab;

import com.epam.lab.view.FlowerShopView;

/**
 * Flower shop program!
 *
 * @author Sofiia Reminska
 */
public class App {
    public static void main(String[] args) {
        new FlowerShopView().show();
    }
}
