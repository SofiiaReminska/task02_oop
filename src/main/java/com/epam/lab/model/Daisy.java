package com.epam.lab.model;

public class Daisy extends Flower {

    private static final int PRICE = 15;

    private static final String WHITE = "White";

    {
        setColor(WHITE);
        setPrice(PRICE);
    }
}
