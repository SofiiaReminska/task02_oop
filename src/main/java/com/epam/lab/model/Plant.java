package com.epam.lab.model;

import java.util.Objects;

/**
 * Represents model of plant.
 */
public abstract class Plant {
    /**
     * price - cost of plant.
     */
    private int price;

    /**
     * Getter for price.
     *
     * @return - price of plant
     */
    public int getPrice() {
        return price;
    }

    /**
     * Setter for flower price.
     *
     * @param price - price of plant
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * Override method equals().
     *
     * @param o - object of type object
     * @return - true or false comparing price
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Plant plant = (Plant) o;
        return price == plant.price;
    }

    /**
     * Override method hashCode().
     *
     * @return - hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(price);
    }
}
