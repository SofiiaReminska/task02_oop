package com.epam.lab.model;

public class Cactus extends Plant {

    private static final int PRICE = 150;

    {
        setPrice(PRICE);
    }
}
