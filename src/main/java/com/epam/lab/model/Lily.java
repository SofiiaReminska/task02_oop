package com.epam.lab.model;

public class Lily extends Flower {

    private static final int PRICE = 30;
    private static final String YELLOW = "Yellow";

    {
        setColor(YELLOW);
        setPrice(PRICE);
    }
}
