package com.epam.lab.model;

public class Ficus extends Plant {

    private static final int PRICE = 250;

    {
        setPrice(PRICE);
    }
}
