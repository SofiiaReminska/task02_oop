package com.epam.lab.model;

public class Rose extends Flower {

    private static final int PRICE = 35;

    private static final String RED = "Red";

    {
        setColor(RED);
        setPrice(PRICE);
    }
}
