package com.epam.lab.model;

import java.util.Map;
import java.util.TreeMap;

/**
 * Represent model of bouquet.
 */
public class Bouquet {
    /**
     * map - instance of TreeMap.
     */
    private Map<Flower, Integer> map = new TreeMap<Flower, Integer>();

    /**
     * Getter for map.
     *
     * @return - map of flowers
     */
    public Map<Flower, Integer> getMap() {
        return map;
    }
}
