package com.epam.lab.model;

public class Kalanchoe extends Plant {

    private static final int PRICE = 100;

    {
        setPrice(PRICE);
    }
}
