package com.epam.lab.model;

public class Hibiscus extends Plant {

    private static final int PRICE = 220;

    {
        setPrice(PRICE);
    }
}
