package com.epam.lab.model;

public class Dracaena extends Plant {

    private static final int PRICE = 300;

    {
        setPrice(PRICE);
    }
}
