package com.epam.lab.model;

public class Gerbera extends Flower {

    private static final int PRICE = 25;

    private static final String PINK = "Pink";

    {
        setColor(PINK);
        setPrice(PRICE);
    }
}
