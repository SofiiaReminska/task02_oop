package com.epam.lab.model;

import java.util.Objects;

/**
 * Represents model of flower.
 */
public abstract class Flower implements Comparable<Flower> {
    /**
     * price - cost of flower.
     */
    private int price;

    /**
     * color - color of flower.
     */
    private String color;

    /**
     * Getter for price.
     *
     * @return - price of flower
     */
    public int getPrice() {
        return price;
    }

    /**
     * Getter for color.
     *
     * @return - color of plant
     */
    public String getColor() {
        return color;
    }

    /**
     * Setter for flower color.
     *
     * @param color - flower color
     */
    void setColor(String color) {
        this.color = color;
    }

    /**
     * Setter for flower price.
     *
     * @param price - price of flower
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * Override method toString().
     *
     * @return - simple name of class type flower
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    /**
     * Override method equals().
     *
     * @param o - object of type object
     * @return - true or false comparing price and color
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flower flower = (Flower) o;
        return price == flower.price
                && Objects.equals(color, flower.color);
    }

    /**
     * Override method hashCode().
     *
     * @return - hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(price, color);
    }

    /**
     * Override method compareTo().
     *
     * @param o - object of type Flower
     * @return - -1,1,0 depending on comparing object
     */
    @Override
    public int compareTo(Flower o) {
        if (price < o.price) return -1;
        else if (price == o.price) return 0;
        return 1;
    }
}
