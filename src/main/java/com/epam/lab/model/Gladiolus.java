package com.epam.lab.model;

public class Gladiolus extends Flower {

    private static final int PRICE = 20;

    private static final String PURPLE = "Purple";

    {
        setColor(PURPLE);
        setPrice(PRICE);
    }
}
