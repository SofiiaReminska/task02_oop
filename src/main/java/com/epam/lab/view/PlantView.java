package com.epam.lab.view;

import com.epam.lab.controller.PlantController;
import com.epam.lab.model.*;

import java.util.LinkedHashMap;

/**
 * Represents methods for displaying plant.
 */
public class PlantView extends AbstractView {
    /**
     * controller - object of type PlantController for getting actions on plant.
     */
    PlantController controller = new PlantController();

    /**
     * Constructor for creating plant view.
     */
    public PlantView() {

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Hibiscus");
        menu.put("2", "  2 - Ficus");
        menu.put("3", "  3 - Dracaena");
        menu.put("4", "  4 - Cactus");
        menu.put("5", "  5 - Kalanchoe");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::press1);
        methodsMenu.put("2", this::press2);
        methodsMenu.put("3", this::press3);
        methodsMenu.put("4", this::press4);
        methodsMenu.put("5", this::press5);

    }

    private void press1() {
        printBoughtMessage(new Hibiscus());
    }

    private void press2() {
        printBoughtMessage(new Ficus());
    }

    private void press3() {
        printBoughtMessage(new Dracaena());
    }

    private void press4() {
        printBoughtMessage(new Cactus());
    }

    private void press5() {
        printBoughtMessage(new Kalanchoe());
    }

    /**
     * Method for printing bill message.
     *
     * @param plant - object of type Plant
     */
    private void printBoughtMessage(Plant plant) {
        System.out.println("You bought " + controller.getInfo(plant));
    }

}
