package com.epam.lab.view;

/**
 * Interface for printing.
 */
public interface Printable {
    /**
     * Method for printing info.
     */
    void print();
}
