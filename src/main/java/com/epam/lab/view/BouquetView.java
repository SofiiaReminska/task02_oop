package com.epam.lab.view;

import com.epam.lab.controller.BouquetController;
import com.epam.lab.model.*;

import java.util.LinkedHashMap;

/**
 * Represents methods for displaying bouquet.
 */
public class BouquetView extends AbstractView {
    /**
     * controller - object of type BouquetController.
     */
    BouquetController controller = new BouquetController();

    /**
     * Constructor for creating bouquet view.
     */
    public BouquetView() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Rose");
        menu.put("2", "  2 - Lily");
        menu.put("3", "  3 - Gerbera");
        menu.put("4", "  4 - Daisy");
        menu.put("5", "  5 - Gladiolus");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::press1);
        methodsMenu.put("2", this::press2);
        methodsMenu.put("3", this::press3);
        methodsMenu.put("4", this::press4);
        methodsMenu.put("5", this::press5);
        methodsMenu.put("Q", this::press6);
    }

    private void press1() {
        addToBouquet(new Rose());
    }

    private void press2() {
        addToBouquet(new Lily());
    }

    private void press3() {
        addToBouquet(new Gerbera());
    }

    private void press4() {
        addToBouquet(new Daisy());
    }

    private void press5() {
        addToBouquet(new Gladiolus());
    }

    /**
     * Method for printing bill.
     */
    private void press6() {
        System.out.println("You bought a bouquet of: ");
        System.out.println(controller.getBouquetMap());
        System.out.print("Total price of bouquet: $" +
                controller.totalPriceOfBouquet());
    }

    /**
     * Method for adding flower to bouquet controller.
     *
     * @param flower - object of type Flower
     */
    private void addToBouquet(Flower flower) {
        System.out.printf("choose amount of %s :%n",
                flower.getClass().getSimpleName());
        int amount = input.nextInt();
        controller.addToBouquet(flower, amount);
    }
}
