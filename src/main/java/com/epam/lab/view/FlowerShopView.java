package com.epam.lab.view;

import java.util.LinkedHashMap;

/**
 * Represents methods for displaying flower shop.
 */
public class FlowerShopView extends AbstractView {
    /**
     * Constructor for creating shop menu view.
     */
    public FlowerShopView() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - choose a bouquet");
        menu.put("2", "  2 - choose a plant");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
    }

    /**
     * Method for displaying BouquetView.
     */
    private void pressButton1() {
        new BouquetView().show();
    }

    /**
     * Method for displaying PlantView.
     */
    private void pressButton2() {
        new PlantView().show();
    }
}
