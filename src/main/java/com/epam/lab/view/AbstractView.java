package com.epam.lab.view;

import java.util.Map;
import java.util.Scanner;

/**
 * Represents methods for displaying information.
 */
public abstract class AbstractView {
    /**
     * menu - object of type Map<String, String>.
     */
    Map<String, String> menu;

    /**
     * methodsMenu - object of type Map<String, Printable>.
     */
    Map<String, Printable> methodsMenu;
    /**
     * input - object of type Scanner.
     */
    static Scanner input = new Scanner(System.in);

    /**
     * Method for displaying menu point.
     */
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                // do nothing
            }
        } while (!keyMenu.equalsIgnoreCase("Q"));
    }

    /**
     * Method for printing menu points.
     */
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

}
