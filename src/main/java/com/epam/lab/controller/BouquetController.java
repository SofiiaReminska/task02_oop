package com.epam.lab.controller;

import com.epam.lab.model.Bouquet;
import com.epam.lab.model.Flower;

import java.util.Map;

/**
 * Represents actions with bouquet.
 *
 * @author Sofiia Reminska
 */
public class BouquetController {
    /**
     * bouquet - instance of Bouquet.
     */
    private Bouquet bouquet = new Bouquet();

    /**
     * Method for adding flowers to bouquet.
     *
     * @param flower - object of type Flower
     * @param amount - amount of flowers of specific type
     */
    public void addToBouquet(Flower flower, int amount) {
        if (bouquet.getMap().containsKey(flower)) {
            amount += bouquet.getMap().get(flower);
        }
        bouquet.getMap().put(flower, amount);
    }

    /**
     * Method for counting price of bouquet.
     *
     * @return - price of bouquet
     */
    public int totalPriceOfBouquet() {
        int totalPrice = 0;
        for (Map.Entry<Flower, Integer> entry : bouquet.getMap().entrySet()) {
            totalPrice += entry.getKey().getPrice() * entry.getValue();
        }
        return totalPrice;
    }

    /**
     * Method for converting bouquet map to string.
     *
     * @return - map of bouquet converted to string
     */
    public String getBouquetMap() {
        return bouquet.getMap().toString();
    }
}
