package com.epam.lab.controller;

import com.epam.lab.model.Plant;

/**
 * Represent actions with plant.
 */
public class PlantController {
    /**
     * Method for getting information about plant name and price.
     *
     * @param plant - object of type Plant
     * @return - formatted information about plant
     */
    public String getInfo(Plant plant) {
        return String.format("%s $%d",
                plant.getClass().getSimpleName(), plant.getPrice());
    }
}
